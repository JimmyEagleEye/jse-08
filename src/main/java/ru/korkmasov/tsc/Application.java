package ru.korkmasov.tsc;


import ru.korkmasov.tsc.constant.ArgumentConst;
import ru.korkmasov.tsc.constant.TerminalConst;
import ru.korkmasov.tsc.model.Command;
import ru.korkmasov.tsc.repository.CommandRepository;
import ru.korkmasov.tsc.util.NumberUtil;
import java.util.Scanner;

public class Application {

    private static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    private static void displayHelp() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            System.out.println(command);
        }
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("Djalal Korkmasov");
        System.out.println("dkorkmasov@tsc.com");
    }

    private static void showCommands(){
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            final String name = command.getName();
            if (name != null) System.out.println(name);
        }
    }

    private static void showArguments(){
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (Command command : commands) {
            final String arg = command.getArg();
            if (arg != null) System.out.println(arg);
        }
    }

    private static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void displayWait() {
        System.out.println();
        System.out.println("Enter command.");
    }

    private static void displayError() {
        System.out.println("Incorrect command. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            displayWait();
            command = scanner.nextLine();
            parseCommand(command);
        }
    }


    private static boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        if (args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                showSystemInfo();
                break;
            default:
                showIncorrectArgument();
        }
    }

    private static void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_INFO:
                showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                showIncorrectCommand();
        }
    }

    private static void showSystemInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    private static void showIncorrectArgument() {
        System.out.println("Error! Argument not found!");
    }

    private static void showIncorrectCommand() {
        System.out.println("Error! Command not found!");
    }

    private static void exit() {
        System.exit(0);
    }

    public static void main(final String[] args) {
        displayWelcome();
        if (parseArgs(args)) System.exit(0);
        process();
    }
}
