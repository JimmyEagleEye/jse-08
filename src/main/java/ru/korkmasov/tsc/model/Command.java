package ru.korkmasov.tsc.model;

import ru.korkmasov.tsc.constant.ArgumentConst;
import ru.korkmasov.tsc.constant.TerminalConst;

public class Command {
    private String arg = "";
    private String description = "";
    private String name = "";

    public Command() {

    }

    public Command(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public String getArg() {
        return arg;
    }

    /*public void setArg(String arg) {
        Command.arg = arg;
    }*/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    /*public void setDescription(String description) {
        Command.description = description;
    }*/

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (arg != null && !arg.isEmpty()) result += "[" + arg + "]";
        if (description != null && !description.isEmpty()) result += " - " + description;
        return result;
    }
}
